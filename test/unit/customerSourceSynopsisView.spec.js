import CustomerSourceSynopsisView from '../../src/customerSourceSynopsisView';
import dummy from '../dummy';

describe('CustomerSourceSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new CustomerSourceSynopsisView(null, dummy.customerSourceName);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new CustomerSourceSynopsisView(expectedId, dummy.customerSourceName);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new CustomerSourceSynopsisView(dummy.customerSourceId, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.customerSourceName;

            /*
             act
             */
            const objectUnderTest =
                new CustomerSourceSynopsisView(dummy.customerSourceId, expectedName);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });

    })
});
