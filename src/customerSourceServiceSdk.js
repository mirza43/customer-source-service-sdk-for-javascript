import CustomerSourceServiceSdkConfig from './customerSourceServiceSdkConfig';
import DiContainer from './diContainer';
import CustomerSourceSynopsisView from './customerSourceSynopsisView';
import ListCustomerSourcesFeature from './listCustomerSourcesFeature';

/**
 * @class {CustomerSourceServiceSdk}
 */
export default class CustomerSourceServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {CustomerSourceServiceSdkConfig} config
     */
    constructor(config:CustomerSourceServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * @param {string} accessToken
     * @returns {Array<CustomerSourceSynopsisView>}
     */
    listCustomerSources(accessToken:string):Array<CustomerSourceSynopsisView> {

        return this
            ._diContainer
            .get(ListCustomerSourcesFeature)
            .execute(accessToken);

    }

}
