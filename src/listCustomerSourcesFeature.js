import {inject} from 'aurelia-dependency-injection';
import CustomerSourceServiceSdkConfig from './customerSourceServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import CustomerSourceSynopsisView from './customerSourceSynopsisView';

@inject(CustomerSourceServiceSdkConfig, HttpClient)
class ListCustomerSourcesFeature {

    _config:CustomerSourceServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:CustomerSourceServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all customer sources
     * @param {string} accessToken
     * @returns {Promise.<CustomerSourceSynopsisView[]>}
     */
    execute(accessToken:string):Promise<Array> {

            return this._httpClient
                .createRequest('customer-sources')
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                    return  Array.from(
                            response.content,
                            contentItem =>
                                new CustomerSourceSynopsisView(
                                    contentItem.id,
                                    contentItem.name
                                )
                        );
                });
    }
}

export default ListCustomerSourcesFeature;
